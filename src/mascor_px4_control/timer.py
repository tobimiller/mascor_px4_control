import rospy

class Timeout:
    def __init__(self, timeout=60.0):
        self.time_init = rospy.Time.now()
        self.timeout = timeout

    def timed_out(self):
        time_now = rospy.Time.now()
        time_diff = (time_now - self.time_init).to_sec()
        if time_diff >= self.timeout:
            return True
        return False

    def set_timeout(self, timeout):
        self.timeout = timeout

    def reset(self):
        self.time_init = rospy.Time.now()
