# -*- coding: latin-1 -*-

###
#   Copyright (c) 2020 FH Aachen. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

import rospy

## -- Messages
from sensor_msgs.msg import NavSatFix
from geometry_msgs.msg import PoseStamped
from mavros_msgs.msg import State
from mavros_msgs.msg import ExtendedState
from mavros_msgs.msg import Altitude

class PreflightHandler:
    """Handler for preflight checks.

    Attributes:
        ns        (str): mavros instance namespace
        logger (object): instance of LogHandler()
    """
    def __init__(self, ns, logger):
        self.__namespace = ns
        self.logger = logger
        

    def check(self):
        """Check if MAV is ready for takeoff.

        Returns:
            bool : True if ready, False otherwise.
        """

        self.logger.logwarn("[PREFLIGHT-CHECK] ...")
        try:
            rospy.wait_for_message(self.__namespace + '/mavros/state', State, timeout=5.0)
            SIGNAL_GOOD = True
            self.logger.loginfo('[FCU connection] ✓')
        except:
            SIGNAL_GOOD = False
            err_str = "Connection to FCU not possible. Shutting down."
            self.logger.logerr(err_str)

        
        if SIGNAL_GOOD:
            try:
                rospy.wait_for_message(self.__namespace + '/mavros/extended_state', ExtendedState, timeout=5.0)
                SIGNAL_GOOD = True
            except:
                SIGNAL_GOOD = False
                err_str = "No Extended State available. Shutting down."
                self.logger.logerr(err_str)
        
        if SIGNAL_GOOD:
            try:
                rospy.wait_for_message(self.__namespace + '/mavros/altitude', Altitude, timeout=5.0)
                SIGNAL_GOOD = True
            except:
                SIGNAL_GOOD = False
                err_str = "No altitude available. Shutting down."
                self.logger.logerr(err_str)

        if SIGNAL_GOOD:
            try:
                rospy.wait_for_message(self.__namespace + '/mavros/global_position/global', NavSatFix, timeout=5.0)
                SIGNAL_GOOD = True
            except:
                SIGNAL_GOOD = False
                err_str = "No GPS available. Shutting down."
                self.logger.logerr(err_str)

        if SIGNAL_GOOD:
            try:
                rospy.wait_for_message(self.__namespace + '/mavros/local_position/pose', PoseStamped, timeout=5.0)
                SIGNAL_GOOD = True
            except:
                SIGNAL_GOOD = False
                err_str = "No local position available. Shutting down."
                self.logger.logerr(err_str)
                
        if SIGNAL_GOOD:
            return True
        else:
            rospy.signal_shutdown(err_str)
            return False
    