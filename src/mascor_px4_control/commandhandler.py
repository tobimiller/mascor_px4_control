# -*- coding: latin-1 -*-

###
#   Copyright (c) 2020 FH Aachen. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

import threading
import numpy as np

import rospy

## -- Messages
from mavros_msgs.msg import OverrideRCIn

## -- Services
from mavros_msgs.srv import CommandBool
from mavros_msgs.srv import CommandLong
from mavros_msgs.srv import CommandTOL
from mavros_msgs.srv import CommandHome
from mavros_msgs.srv import SetMode
from mavros_msgs.srv import ParamGet

class CommandHandler:
    """Provides functions to send commands to the px4 firmware.

    Attributes:
        ns (str)                            : namespace of mavros instance
        localization (LocalizationHandler()): instance of LocalizationHandler()
        state (StateHandler())              : instance of StateHandler()
        logger (LogHandler())               : instance of LogHandler()
    """
    def __init__(self, ns, localization, state, logger):
        ## -- Variables
        self.__namespace = ns
        self.localization = localization
        self.state = state
        self.logger = logger

        ## -- AUX-MAPPING
        self.__aux_mapping = []
        #wait for param to get available
        while True:
            try:
                rc_channel = self.get_param("RC_MAP_AUX1").integer
                break
            except AttributeError:
                rospy.sleep(1)
        for i in range(1,7):
            rc_channel = self.get_param("RC_MAP_AUX"+str(i)).integer  
            self.__aux_mapping.append(rc_channel)

        ## -- Publisher
        self.__rc_override = rospy.Publisher(self.__namespace + '/mavros/rc/override', OverrideRCIn, queue_size=1)

        ## -- Thread
        self.__kill_thread = False
        self.__thread_killed = False
        channels = [OverrideRCIn.CHAN_NOCHANGE for _ in range(8)]
        self.__rc_override_msg = OverrideRCIn(channels=channels)
        self.lock_rc_msg = threading.Lock()
        rc_override_publisher = threading.Thread(target=self.__rc_override_loop, args=())
        rc_override_publisher.start()


    ## -- THREAD-LOOP
    def __rc_override_loop(self):
        """Publisher loop for RC-Override message. 
        """
        rate = rospy.Rate(25)
        while not self.__kill_thread:
            self.lock_rc_msg.acquire()
            try:
                self.__rc_override.publish(self.__rc_override_msg)
                rate.sleep()
            except:
                self.logger.logerr("RC Override publisher error.")
                pass
            self.lock_rc_msg.release()
        self.__thread_killed = True

    def thread_killed(self):
        """Returns info about killed thread.

        Returns:
            bool: if thread was killed 
        """
        return self.__thread_killed

    def kill_thread(self):
        """Sets kill-flag for threads.
        """
        self.__kill_thread = True
        rate = rospy.Rate(10)
        while not self.__thread_killed:
            rate.sleep()


    ## -- SET FUNCTIONS
    def __cmd_srv(self, cmd_id, p1=None, p2=None, p3=None, p4=None, p5=None, p6=None, p7=None):
        """Sends command by id and parameters.
        http://docs.ros.org/api/mavros_msgs/html/msg/CommandCode.html
        https://mavlink.io/en/messages/common.html#mav_commands

        Arguments:
            cmd_id (int): id of command
            p1-p7  (float)   : param1-7 for specific command
        """
        br = False
        confirmation = 0
        try:
            rospy.wait_for_service(self.__namespace + '/mavros/cmd/command', timeout=5)
        except:
            self.logger.logerr("[SEND CMD] timeout.")
        try:
            cmd = rospy.ServiceProxy(self.__namespace + '/mavros/cmd/command', CommandLong)
            cmd(br, cmd_id, confirmation, p1, p2, p3, p4, p5, p6, p7)
        except rospy.ServiceException, e:
            s = "Service call failed: %s"%e
            self.logger.logerr(s)


    def set_flightmode(self, flightmode):
        """Sets flightmode of fcu
        http://wiki.ros.org/mavros/CustomModes
        
        Arguments:
            flightmode (str):   MANUAL
                                ACRO
                                ALTCTL
                                POSCTL
                                OFFBOARD
                                STABILIZED
                                RATTITUDE
                                AUTO.MISSION
                                AUTO.LOITER disable RC failsafe, which can be done by setting NAV_RCL_ACT parameter to 0
                                AUTO.RTL
                                AUTO.LAND
                                AUTO.RTGS
                                AUTO.READY
                                AUTO.TAKEOFF
        Returns:
            bool : if flightmode was set
        """
        try:
            rospy.wait_for_service(self.__namespace + '/mavros/set_mode', timeout=5)
        except:
            self.logger.logerr("[SET FLIGHTMODE] timeout.")
            return False
        try:
            srvSetMode = rospy.ServiceProxy(self.__namespace + '/mavros/set_mode', SetMode)
            resp = srvSetMode(0, flightmode)
            return resp.mode_sent
        except rospy.ServiceException, e:
            s = "Service call failed: %s"%e
            self.logger.logerr(s)
            return False

    def set_offboard(self):
        """Set flightmode to offboard.

        Returns:
            bool : if offboard mode was set.
        """
        return self.set_flightmode("OFFBOARD")

    def set_home(self, curr_gps=True, yaw=0., lat=0., lon=0., alt=0.):
        """Set home position

        Arguments:
            curr_gps (bool) : True/False if actual gps should be used
            yaw      (float): Yaw (rad)
            lat      (float): Latitude(deg)
            lon      (float): Longitude(deg)
            alt      (float): Altitude(m)

        Returns:
            bool : if set home was set.
        """
        try:
            rospy.wait_for_service(self.__namespace + '/mavros/cmd/set_home', timeout=5)
        except:
            self.logger.logerr("[SET HOME] timeout.")
            return False
        try:
            home = rospy.ServiceProxy(self.__namespace + '/mavros/cmd/set_home', CommandHome)
            resp = home(curr_gps, yaw, lat, lon, alt)
            return resp.success
        except rospy.ServiceException, e:
            s = "Service call failed: %s"%e
            self.logger.logerr(s)
            return False


    def set_aux_channel(self, channel, pwm):
        """Set AUX channel output.

        Arguments:
            channel (int): AUX channel
            pwm     (int): PWM value
        """
        rc_channel = self.__aux_mapping[channel-1]
        if rc_channel != 0:
            if pwm != OverrideRCIn.CHAN_NOCHANGE and pwm != OverrideRCIn.CHAN_RELEASE:
                if pwm < 900:
                    pwm = 900
                elif pwm > 2000:
                    pwm = 2000
            channels = [OverrideRCIn.CHAN_NOCHANGE for _ in range(8)]
            channels[rc_channel-1] = pwm
            self.lock_rc_msg.acquire()
            self.__rc_override_msg = OverrideRCIn(channels=channels)
            self.lock_rc_msg.release()
        else:
            self.logger.logwarn("AUX-Output is not mapped to a RC-Channel")


    def return_to_home(self):
        """Send RTL command to fcu"""
        self.__cmd_srv(20)


    def arm(self):
        """Send arm command to fcu."""
        self.set_flightmode("MANUAL")
        self.__arming(True)


    def disarm(self):
        """Send disarm command to fcu."""
        self.__arming(False)


    def __arming(self, boolean):
        """Arming function.

        Arguments:
            boolean (bool): True/False for arm/disarm
        """
        if boolean == self.state.armed():
            if boolean == True:
                self.logger.loginfo("Already armed.")
            else:
                self.logger.loginfo("Already disarmed.")
        else:
            if self.state.landed():
                try:
                    rospy.wait_for_service(self.__namespace + '/mavros/cmd/arming', timeout=5)
                except:
                    self.logger.logerr("[ARMING] timeout.")
                try:
                    arming = rospy.ServiceProxy(self.__namespace + '/mavros/cmd/arming', CommandBool)
                    arming(boolean)
                except rospy.ServiceException, e:
                    s = "Service call failed: %s"%e
                    self.logger.logerr(s)
            else:
                self.logger.logwarn("Not on ground. Dis-/Arming not possible!")


    def takeoff(self, altitude, yaw=0.0):
        """Send takeoff command to fcu.

        Arguments:
            altitude (float): altitude(m) above starting point
            yaw      (float): yaw(rad) in ENU frame 
        """
        if self.state.in_air():
            self.logger.logwarn("[TAKEOFF] ❌. Already in air.")
            return False
        self.set_home(True)
        rate = rospy.Rate(1)
        while not self.state.armed():
            self.arm()
            rate.sleep()

        if (self.localization.has_gp_fix() and self.state.armed()):
            try:
                rospy.wait_for_service(self.__namespace + '/mavros/cmd/takeoff', timeout=5)
            except:
                self.logger.logerr("[TAKEOFF] timeout.")
                return False
            yaw = (np.pi/2) - yaw # NED to ENU Conversion
            try:
                takeoff = rospy.ServiceProxy(self.__namespace + '/mavros/cmd/takeoff', CommandTOL)
                takeoff(0, yaw, self.localization.get_gp_msg().latitude, self.localization.get_gp_msg().longitude, self.localization.get_asml() + altitude)
                self.logger.loginfo("[TAKEOFF] " + str(altitude) + "m over ground.")
            except rospy.ServiceException, e:
                s = "Service call failed: %s"%e
                self.logger.logerr(s)
                return False
        else:
            self.logger.logwarn("[GPS] ❌. Takeoff not possible")
            return False
    

    def land(self, altitude=0.0, yaw=0.0):
        """Send land command to fcu.

        Arguments:
            altitude (float): altitude(m) above starting point
            yaw      (float): yaw(rad) in reference frame 
        """
        
        if (self.localization.has_gp_fix() and self.state.armed()):
            try:
                rospy.wait_for_service(self.__namespace + '/mavros/cmd/land', timeout=5)
            except:
                self.logger.logerr("[LANDING] timeout.")
                return False
            try:
                land = rospy.ServiceProxy(self.__namespace + '/mavros/cmd/land', CommandTOL)
                land(0, yaw, self.localization.get_gp_msg().latitude, self.localization.get_gp_msg().longitude, self.localization.get_asml() + altitude)
                self.logger.loginfo("[LANDING] " + str(altitude) + "m over ground")
                return True
            except rospy.ServiceException, e:
                s = "Service call failed: %s"%e
                self.logger.logerr(s)
                return False
        else:
            self.logger.logwarn("[GPS] ❌. Landing not possible.")
            return False

    ## -- GET FUNCTIONS
    def get_param(self, param_name):
        """Get parameter values

        Arguments:
            param_name (str): name of parameter

        Returns:
            float: value 
        """
        try:
            rospy.wait_for_service(self.__namespace + '/mavros/param/get', timeout=5)
        except:
            self.logger.logerr("[GET PARAM] timeout.")
            return None
        try:
            srvGetParam = rospy.ServiceProxy(self.__namespace + '/mavros/param/get', ParamGet)
            resp = srvGetParam(param_name)
            if resp.success:
                return resp.value
            else:
                return None
        except:
            return None
