# -*- coding: latin-1 -*-

###
#   Copyright (c) 2020 FH Aachen. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

import rospy

## -- Messages
from mavros_msgs.msg import WaypointReached
from mavros_msgs.msg import WaypointList
from mavros_msgs.msg import Waypoint
from mavros_msgs.msg import CommandCode

## -- Services
from mavros_msgs.srv import WaypointPull, WaypointPullRequest
from mavros_msgs.srv import WaypointPush, WaypointPushRequest
from mavros_msgs.srv import WaypointClear, WaypointClearRequest
from mavros_msgs.srv import WaypointSetCurrent, WaypointSetCurrentRequest


class MissionHandler:
    """Handler for missions.

    Attributes:
        ns (str): namespace of mavros instance
    """
    def __init__(self, ns):
        ## -- Variables
        self.wp_list = []
        self.wp_reached_msg = WaypointReached(wp_seq=-1)

        ## -- SUBSCRIBER
        rospy.Subscriber(ns + "/mavros/mission/reached", WaypointReached, self.__cb_wp_reached)
        rospy.Subscriber(ns + "/mavros/mission/waypoints", WaypointList, self.__cb_wp_list)

        ## -- SERVICES
        self.srv_pull = rospy.ServiceProxy(ns + '/mavros/mission/pull', WaypointPull)
        self.srv_push = rospy.ServiceProxy(ns + '/mavros/mission/push', WaypointPush)
        self.srv_clear = rospy.ServiceProxy(ns + '/mavros/mission/clear', WaypointClear)
        self.srv_set_current = rospy.ServiceProxy(ns + '/mavros/mission/set_current', WaypointSetCurrent)


    def __cb_wp_reached(self, msg):
        """Callback of waypoint reached subscriber.
        """
        self.wp_reached_msg = msg

    def __cb_wp_list(self, msg):
        """Callback of waypoint list subscriber.
        """
        self.wp_list_msg = msg

    def pull(self):
        """Requests waypoints from device
        
        Returns:
            mavros_msgs.msg.WaypointPullResponse() : success status and received count
        """
        return self.srv_pull()

    def push(self, start_seq=0, waypoint_list=None):
        """Send waypoints to device

        Arguments:
            start_seq (int) : will define a partial waypoint update. Set to 0 for full update
            waypoint_list (np.ndarray, list, tuple) : list of mavros_msgs.msg.Waypoint() 

        Returns:
            mavros_msgs.msg.WaypointPushResponse() : response from waypoint push service
        """
        if waypoint_list is None:
            waypoint_list = self.wp_list
        req = WaypointPushRequest(start_index=start_seq, waypoints=waypoint_list)
        return self.srv_push(req)

    def clear(self):
        """Request clear waypoint.

        Returns:
            bool : success
        """
        return self.srv_clear().success

    def set_current(self, seq):
        """Request set current waypoint.

        Arguments:
            seq (int): index in waypoint array

        Returns:
            bool : success
        """
        return self.srv_set_current(seq).success

    def add_waypoint(self, lat, lon, alt,
                    seq=-1,
                    frame=Waypoint.FRAME_GLOBAL_REL_ALT,
                    command=CommandCode.NAV_WAYPOINT,
                    autocontinue=True,
                    param1=0.0,
                    param2=0.0,
                    param3=0.0,
                    param4=0.0):
        """Add waypoint to waypoint list.

        Arguments:
            lat   (float): latitude (deg)
            lon   (float): longitude (deg)
            alt   (float): altitude relative to frame(m)
            seq     (int): replace waypoint at this position
            frame   (int): http://docs.ros.org/melodic/api/mavros_msgs/html/msg/Waypoint.html
            command (int): http://docs.ros.org/melodic/api/mavros_msgs/html/msg/CommandCode.html
            autocontinue (bool): TODO
            param1-4    (float): https://mavlink.io/en/messages/common.html#MAV_CMD_NAV_WAYPOINT
        """
        wp_msg = Waypoint()
        wp_msg.frame = frame
        wp_msg.command = command
        wp_msg.autocontinue = autocontinue
        wp_msg.param1 = param1
        wp_msg.param2 = param2
        wp_msg.param3 = param3
        wp_msg.param4 = param4
        wp_msg.x_lat = lat
        wp_msg.y_long = lon
        wp_msg.z_alt = alt
        if seq < 0:    
            self.wp_list.append(wp_msg)
        else:
            self.wp_list[seq] = wp_msg

    def delete_waypoint(self, seq):
        """Deletes waypoint from list. 
        Has to be pushed again!

        Arguments:
            seq (int): waypoint from list at position seq will be deleted
        """
        del self.wp_list[seq]

    def get_waypoint_list(self):
        """Returns waypoint list message

        Returns:
            mavros_msgs.msg.WaypointList() : list of waypoints
        """
        return self.wp_list_msg

    def get_reached_msg(self):
        """Returns waypoint reached message

        Returns:
            mavros_msgs.msg.WaypointReached() : reached waypoint message
        """
        return self.wp_reached_msg

    def reached_waypoint(self, seq):
        """Returns if waypoint is reached.

        Arguments:
            seq (int): waypoint sequence id

        Returns:
            bool : True if waypoint sequence id is reached, False otherwise
        """
        if seq == self.wp_reached_msg.wp_seq:
            return True
        else:
            return False
