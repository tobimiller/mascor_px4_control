#!/usr/bin/env python

import sys
from mascor_px4_control import geo_conversions as gc

import rospy
import tf2_ros
import tf2_geometry_msgs
import tf
from geometry_msgs.msg import PoseStamped, TransformStamped
from sensor_msgs.msg import NavSatFix


class Subscriber:
    def __init__(self, namespace, origin):
        self.ns = namespace
        self.origin = origin
        self.gp_msg = NavSatFix()
        self.lp_msg = PoseStamped()
        self.lp_msg.pose.orientation.w = 1
        self.gp_pos = [0, 0, 0]
        rospy.Subscriber("/" + self.ns + "/mavros/global_position/global", NavSatFix, self.callbackGlobalPosition)
        rospy.Subscriber("/" + self.ns + "/fix", NavSatFix, self.callbackGlobalPosition)
        rospy.Subscriber("/" + self.ns + "/mavros/local_position/pose", PoseStamped, self.callbackLocalPositon)
        if self.ns == "": # will add prefix for tf names when namespace is empty
            self.ns = "uav"

    ####
    ## Callbacks
    ####

    def callbackGlobalPosition(self, msg):
        self.gp_msg = msg
        self.gp2xy(msg)

    def callbackLocalPositon(self, msg):
        self.lp_msg = msg

    def gp2xy(self, gp_msg):
        x, y = gc.ll2xy(gp_msg.latitude, gp_msg.longitude, self.origin[0], self.origin[1])
        self.gp_pos = [x, y, gp_msg.altitude - self.origin[2]]


class FrameSynchronization:
    def __init__(self, namespaces, origin):
        self.origin_frame = "world"
        self.subscribers = []
        # TF
        self.tfBuffer = tf2_ros.Buffer()
        self.tfListener = tf2_ros.TransformListener(self.tfBuffer)
        self.br = tf2_ros.TransformBroadcaster()
        # Subscribers
        for namespace in namespaces:
            self.subscribers.append(Subscriber(namespace, origin))
        # Main
        self.main()


    def generate_gp_transform(self, subscriber):
        trans = TransformStamped()
        trans.header.stamp = rospy.Time.now()
        trans.header.frame_id = self.origin_frame
        trans.child_frame_id = subscriber.ns
        trans.transform.translation.x = subscriber.gp_pos[0]
        trans.transform.translation.y = subscriber.gp_pos[1]
        trans.transform.translation.z = subscriber.gp_pos[2]
        trans.transform.rotation = subscriber.lp_msg.pose.orientation
        return trans


    def generate_lp_transform(self, subscriber):
        trans = TransformStamped()
        trans.header.stamp = rospy.Time.now()
        trans.header.frame_id = subscriber.ns + "_origin"
        trans.child_frame_id = subscriber.ns + "_helper"
        trans.transform.translation.x = subscriber.lp_msg.pose.position.x
        trans.transform.translation.y = subscriber.lp_msg.pose.position.y
        trans.transform.translation.z = subscriber.lp_msg.pose.position.z
        trans.transform.rotation = subscriber.lp_msg.pose.orientation
        return trans


    def main(self):
        rate = rospy.Rate(50)
        while not rospy.is_shutdown():
            for subscriber in self.subscribers:
                trans_gp = self.generate_gp_transform(subscriber)
                self.br.sendTransform(trans_gp)
                trans_lp = self.generate_lp_transform(subscriber)
                self.br.sendTransform(trans_lp)
                try:
                    trans_inv = self.tfBuffer.lookup_transform(trans_lp.child_frame_id, trans_lp.header.frame_id, rospy.Time(0))
                    trans_inv.header.frame_id = subscriber.ns
                    trans_inv.child_frame_id = subscriber.ns + "_origin"
                    if not trans_inv.transform.rotation.w == 0.0:
                        self.br.sendTransform(trans_inv)
                except:
                    rospy.logwarn("Lookup failed.")
                    continue
            rate.sleep()


if __name__ == '__main__':
    try:
        rospy.init_node("FrameSynchronization")
        #ns = rospy.get_param("~namespaces", default=["uav0", "uav1", "uav2"])
        ns = rospy.get_param("~namespaces", default=[""])
        origin  = rospy.get_param("~origin", default=[47.3977423, 8.5456063, 535.338898595]) #simulation
        fs = FrameSynchronization(ns, origin)
        rospy.spin()
    except rospy.ROSInterruptException:
        sys.exit() 
